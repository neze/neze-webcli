|build| |pypi| |docs|

.. |build| image:: https://gitlab.com/neze/neze-webcli/badges/master/pipeline.svg
   :target: https://gitlab.com/neze/neze-webcli/
.. |pypi| image:: https://badge.fury.io/py/neze-webcli.svg
   :target: https://pypi.org/project/neze-webcli/
.. |docs| image:: https://readthedocs.org/projects/neze-webcli/badge/?version=latest
   :target: https://neze-webcli.readthedocs.io/en/latest/
