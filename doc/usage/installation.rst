Installation
============

With PyPi
---------

|pypi|

.. |pypi| image:: https://badge.fury.io/py/neze-webcli.svg
   :target: https://pypi.org/project/neze-webcli/

The ``neze-webcli`` project is available `on pypi`_.

.. _on pypi: https://pypi.org/project/neze-webcli/

.. code-block:: bash

   pip install --upgrade neze-webcli

Note that you can also install the development version.

.. code-block:: bash

   pip install --upgrade --pre neze-webcli

From source
-----------

Just install the requirements listed in ``requirements.txt``, either by hand
or with ``pip``.

.. code-block:: bash

   pip install -r requirements.txt

Development setup
-----------------

Best way to setup your development is probably with a virtual environment.
I use a `virtualenv`_ set up with `virtualenv-wrapper`_.

.. _virtualenv: https://virtualenv.pypa.io/en/stable/
.. _virtualenv-wrapper: https://virtualenvwrapper.readthedocs.io/en/stable/

.. code-block:: bash

   cd /path/to/webcli/source
   mkvirtualenv -p $(which python3) -a $(pwd) webcli
   pip install -e .

Note that if you want to build documentation you will need to install sphinx.

.. code-block:: bash

  workon webcli
  pip install -r requirements.dev.txt
  python setup.py doc
