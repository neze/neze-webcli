Transmission
============

Usage
-----

The ``transmission`` executable simply calls the module :py:mod:`webcli.cli.transmission` to interact with a remote transmission API.

.. code-block:: bash

   which transmission || alias transmission="python3 -m webcli.cli.transmission"
