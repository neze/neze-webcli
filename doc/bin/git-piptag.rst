Git PIPTAG
==========

Usage
-----

The ``git-piptag`` executable simply calls the module :py:mod:`webcli.cli.git_piptag`.

.. code-block:: bash

   which git-piptag || alias git-piptag="python3 -m webcli.cli.git_piptag"

This program has four modes:

root
  Finds the latest version tag in the git tree.

    .. code-block:: bash

       git piptag [--root|-r]

get
  Get the tag of the HEAD. If you do not propose a tag, it is automatically
  calculated depending on the latest version tag and the distance with it in
  the git tree. Using get with a proposed-tag will only have as an effect to
  parse and normalize your tag.

    .. code-block:: bash

       git piptag [--get|-g] [proposed-tag]

set
  Put the tag of the HEAD in the tree. In the default dry-run mode (``-n``) it
  just prints the git command that would be executed. In force mode (``-f``) the
  tag is set by calling ``git tag``.

    .. code-block:: bash

       git piptag [--set|-s] [-n|-f] [proposed-tag]

dev
  Put a ``dev0`` tag in the tree in the same way as **set**. The intended
  meaning is that from this point in the tree you are doing development commits
  for your next version. If you do not propose a tag (of a stable next version),
  the next version number is calculated automatically from the current one. This
  computation is not guaranteed to produce coherent output in some git tree
  states.

    .. code-block:: bash

       git piptag [--dev|-d] [-n|-f] [proposed-tag]

Workflow Example
----------------

.. code-block:: bash

  λ git init .
  λ touch .gitignore
  λ git add .gitignore && git commit -m "init"

    * 6e844c9 (HEAD -> master) init

  λ git piptag # Default root tag is the lowest possible and not very interesting.
  0a0.dev0

  λ touch v1 && git add v1 && git commit -m "release 1"

    * 669e09e (HEAD -> master) release 1
    * 6e844c9 init

  λ git piptag v1.0 -n # We would like to release v1.0. Start with a dry run.
  git tag -s -m 'Automatic v1.0 Tag by Git Piptag' v1.0

  λ git piptag v1.0 -f # We're satisfied. Let's apply this.

    * 669e09e (HEAD -> master, tag: v1.0) release 1
    * 6e844c9 init

  λ git piptag -g # What would be the tag of the current commit now?
  1.0+git.669e09eb

  λ git checkout -b dev
  λ touch todo && git add todo && git commit -m "v2 plan"

    * 5b1c624 (HEAD -> dev) v2 plan
    * 669e09e (tag: v1.0, master) release 1
    * 6e844c9 init

  λ git piptag -g # Wrongly gives a 'post v1' tag while we're developing v2
  1.0.post1+git.5b1c6243

  λ git piptag -d 2.0 -n # Say that we're developing v2
  git tag -s -m 'Automatic v2.0.dev0 Tag by Git Piptag' v2.0.dev0

  λ git piptag -d 2.0 -f

    * 5b1c624 (HEAD -> dev, tag: v2.0.dev0) v2 plan
    * 669e09e (tag: v1.0, master) release 1
    * 6e844c9 init

  λ touch feature && git add feature && git commit -m "new feature"

    * 70f8129 (HEAD -> dev) new feature
    * 5b1c624 (tag: v2.0.dev0) v2 plan
    * 669e09e (tag: v1.0, master) release 1
    * 6e844c9 init

  λ git piptag -g # The tag of the current commit is now more coherent
  2.0.dev1+git.70f8129c

  λ touch v2b && git add v2b && git commit -m "beta release 2"
  λ git piptag 2.0b -f # Tell we're releasing a beta

    * 985edf0 (HEAD -> dev, tag: v2.0b0) beta release 2
    * 70f8129 new feature
    * 5b1c624 (tag: v2.0.dev0) v2 plan
    * 669e09e (tag: v1.0, master) release 1
    * 6e844c9 init

  λ git checkout master
  λ touch fix && git add fix && git commit -m "bugfix"

    * 10710b4 (HEAD -> master) bugfix
    | * 985edf0 (tag: v2.0b0, dev) beta release 2
    | * 70f8129 new feature
    | * 5b1c624 (tag: v2.0.dev0) v2 plan
    |/
    * 669e09e (tag: v1.0) release 1
    * 6e844c9 init

  λ git piptag -g # The tag is now post-release
  1.0.post1+git.10710b40

