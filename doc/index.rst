Neze WEBCLI |release|
=====================

.. include:: ../README.rst

.. toctree::
   :maxdepth: 4
   :caption: Contents
   :glob:

   usage/installation
   usage/*

   package/index
   bin/index

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
