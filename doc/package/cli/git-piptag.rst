Git Piptag CLI
==============

Usage
-----

See :doc:`/bin/git-piptag`

::

  usage: git-piptag [-h] [-r | -s | -d | -g] [-n | -f] [tag]

  Manages git tags for pypi versioning. See PEP440.

  optional arguments:
    -h, --help  show this help message and exit

  action:
    -r, --root  Get and print the latest version tag among the parents commits
                in the git tree. This is the default operation.
    -s, --set   Set version tag in git. This is usually done for a release. Use
                the dry-run mode (-n) before actually running this (-f).
    -d, --dev   Set .dev0 version tag in git. `git piptag -fd 3.0` means that
                your next commits will be tagged v3.0.devN meaning that version
                3.0 is the next release being developed. Do not specify a `.dev`
                version yourself in the command line. Use the dry-run mode (-n)
                before actually running this (-f).
    -g, --get   Get and print the current version tag by offsetting the root
                tag.

  dry run:
    If none of these is selected, the `root` and `get` functions are usable.
    Selecting one of these two modes switches to the `set` and `dev`
    functions.

    -n          Dry Run
    -f          Actually Run

  tag:
    Every tags used by this program follow the PEP440 specification. It is
    available at https://www.python.org/dev/peps/pep-0440/

    tag         Your proposal for a version tag. Should not be a development or
                local tag.

Module contents
---------------

.. automodule:: webcli.cli.git_piptag
   :members:
