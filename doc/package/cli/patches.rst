CLI Patches
===========

Display patch
-------------

.. automodule:: webcli.cli.patches.display
   :members:

Config patch
------------

.. automodule:: webcli.cli.patches.config
   :members:
