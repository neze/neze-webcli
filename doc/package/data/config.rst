Configuration structures
========================

The :py:mod:`webcli.data.config` provides interfaces for working with configuration
files.

Configuration files
-------------------

Usually, there are several configuration levels, in a git-like way. For example
the default :py:class:`webcli.data.config.FileNames` object defines three levels:

  - ``system`` corresponding to ``/etc/<name>.<ext>`` configuration files
  - ``global`` corresponding to ``~/.<name>.<ext>`` or ``~/.config/<name>.<ext>``
  - ``local`` corresponding to ``$(git-dir)/<name>.<ext>``

It supports the default extensions, currently ``json``, ``yaml`` and ``ini``.

The levels are defined in order just like in git, which means that in this example
``global`` values override ``system`` values.

Configuration content
---------------------

Every configuration value is of the form ``<section>.<key>=<value>``, with a
special ``DEFAULT`` section, also shortened as ``<key>=<value>``.

Examples
~~~~~~~~

.. code-block:: ini

    [DEFAULT]
    foo=42

    [prod]
    foo=13

.. code-block:: json

    {
      "DEFAULT": {"foo":42},
      "prod": {"foo":13}
    }

.. code-block:: yaml

    DEFAULT:
      foo: 42
    prod:
      foo: 13

Special keys
~~~~~~~~~~~~

Special configuration keys use other modules to fetch more information.

- ``@secrets``: see :doc:`/package/data/secrets`

.. code-block:: ini

    [DEFAULT]
    @secrets=pass://www/example.com

Configuration specification
---------------------------

The :py:class:`webcli.data.config.Spec` class is used to define configuration
content.

A configuration finding unknown keys or sections will then refuse the faulty file.

By defining the ``DEFAULT`` section, however, it autorizes sections with
arbitrary names that will have to follow the specification of the default
section.

Configuration example
---------------------

.. code-block:: python

    spec = Spec('service')
    spec.add_section('DEFAULT')
    spec.add_key('@secrets',str)
    spec.add_key('user',str)
    spec.add_key('password',str)

    config = Config(spec)

    # Write configuration value in the cache AND in the local configuration file
    config['local.test.user'] = 'anonymous'

    config['global.test.user'] = 'me'

    print(config['test.user'])
    # 'anonymous'


Python module
-------------

.. automodule:: webcli.data.config
   :members:
