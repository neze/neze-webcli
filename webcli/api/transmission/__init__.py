# SPECIFICATION: https://trac.transmissionbt.com/browser/trunk/extras/rpc-spec.txt
from ._api import API as TransmissionAPI

__all__=['TransmissionAPI']
