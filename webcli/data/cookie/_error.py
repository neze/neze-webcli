from .._error import DataError as _Error

__all__=['CookieError']

class CookieError(_Error):
    pass
