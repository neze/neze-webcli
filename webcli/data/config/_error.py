from .._error import DataError as _Error

__all__=['ConfigError']

class ConfigError(_Error):
    pass
