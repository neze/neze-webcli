from .._error import WebcliError as _Error

class DataError(_Error):
    pass
