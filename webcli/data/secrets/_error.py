from .._error import DataError as _Error

__all__ = ['SecretsError']

class SecretsError(_Error):
    pass
