.PHONY: all clean pypi test.pypi ci.pypi doc cov sdist bdist_wheel
DIST:=$(shell python setup.py)
IDENTITY:=$(shell git config user.signingkey)
TWINE:=twine upload --skip-existing

all: ${DIST}

clean:
	rm -rf build dist
	find webcli -type f -name '*.py[cod]' -delete -or -type d -name '__pycache__' -delete

dist/%.whl:
	python setup.py bdist_wheel

dist/%.tar.gz:
	python setup.py sdist

pypi: ${DIST}
	@test -n "${DIST}"
	@TWINE_USERNAME=neze \
		TWINE_PASSWORD="$$(pass www/pypi.org | head -n1 | tr -d '\n')" \
		$(TWINE) \
		--sign --sign-with gpg2 --identity ${IDENTITY} \
		${DIST}

test.pypi: ${DIST}
	@test -n "${DIST}"
	@TWINE_USERNAME=neze \
		TWINE_PASSWORD="$$(pass www/test.pypi.org | head -n1 | tr -d '\n')" \
		TWINE_REPOSITORY_URL=https://test.pypi.org/legacy/ \
		$(TWINE) \
		--sign --sign-with gpg2 --identity ${IDENTITY} \
		${DIST}

ci.pypi: ${DIST}
	@test -n "${DIST}"
	$(TWINE) ${DIST}

sdist bdist_wheel doc cov:
	python setup.py $@

www: doc
	cd build/sphinx/html && tar czf - . | ssh drd.ovh "cd www && rm -rf webcli && mkdir webcli && cd webcli && tar xzf -"
